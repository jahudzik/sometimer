package pl.jahu.sometimer;

import junit.framework.TestCase;

import static junit.framework.Assert.assertEquals;

/**
 * SomeTimer
 * Created by jahudzik on 2015-03-09.
 */
public class TimerUtilsTest extends TestCase {

    public void testFormatMilliseconds_Empty() {
        assertEquals("00:00:00", TimerUtils.formatMilliseconds(0));
    }

    public void testFormatMilliseconds_CoupleMillis() {
        assertEquals("00:00:00", TimerUtils.formatMilliseconds(34));
    }

    public void testFormatMilliseconds_AlmostOneSecond() {
        assertEquals("00:00:00", TimerUtils.formatMilliseconds(999));
    }

    public void testFormatMilliseconds_OneSecond() {
        assertEquals("00:00:01", TimerUtils.formatMilliseconds(1000));
    }

    public void testFormatMilliseconds_OverOneSecond() {
        assertEquals("00:00:01", TimerUtils.formatMilliseconds(1001));
    }

    public void testFormatMilliseconds_AlmostOneMinute() {
        assertEquals("00:00:59", TimerUtils.formatMilliseconds(59999));
    }

    public void testFormatMilliseconds_OneMinute() {
        assertEquals("00:01:00", TimerUtils.formatMilliseconds(60000));
    }

    public void testFormatMilliseconds_OneMinuteAndCoupleSeconds() {
        assertEquals("00:01:02", TimerUtils.formatMilliseconds(62123));
    }

    public void testFormatMilliseconds_AlmostOneHour() {
        assertEquals("00:59:59", TimerUtils.formatMilliseconds(3599000));
    }

    public void testFormatMilliseconds_OneHour() {
        assertEquals("01:00:00", TimerUtils.formatMilliseconds(3600000));
    }

    public void testFormatMilliseconds_HoursMintesAndSeconds() {
        assertEquals("12:37:04", TimerUtils.formatMilliseconds(45424001));
    }

}
