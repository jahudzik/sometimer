package pl.jahu.sometimer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

/**
 * SomeTimer
 * Created by jahudzik on 2015-03-09.
 */
public class TimerActivity extends Activity {

    public static final String TIMER_VALUE_EXTRA_KEY = "timeValue";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, TimerFragment.newInstance())
                    .commit();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(TIMER_VALUE_EXTRA_KEY)) {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
            if (fragment instanceof TimerFragment) {
                ((TimerFragment) fragment).setTimerValue(intent.getStringExtra(TIMER_VALUE_EXTRA_KEY));
            }
        }
    }

}
