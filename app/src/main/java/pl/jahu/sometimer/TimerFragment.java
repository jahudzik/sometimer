package pl.jahu.sometimer;

import android.app.Fragment;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
* SomeTimer
* Created by jahudzik on 2015-03-09.
*/
public class TimerFragment extends Fragment {

    private TimerService timerService;
    private ServiceConnection connection = new TimerServiceConnection();
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;

    private boolean bound = false;
    private boolean isRunning;

    private TextView timerLabel;
    private Button startButton;
    private Button stopButton;

    public static TimerFragment newInstance() {
        return new TimerFragment();
    }

    public TimerFragment() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(TimerService.TIMER_UPDATE_ACTION)) {
                    timerLabel.setText(intent.getStringExtra(TimerService.TIMER_VALUE));
                }
            }
        };
        intentFilter = new IntentFilter(TimerService.TIMER_UPDATE_ACTION);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_timer, container, false);

        timerLabel = (TextView) rootView.findViewById(R.id.label_timer);
        startButton = (Button) rootView.findViewById(R.id.button_start);
        stopButton = (Button) rootView.findViewById(R.id.button_stop);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), TimerService.class);
                getActivity().getApplication().bindService(intent, connection, Context.BIND_AUTO_CREATE);
                isRunning = true;
                startButton.setEnabled(false);
                stopButton.setEnabled(true);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // stop and reset timer
                getActivity().getApplication().unbindService(connection);
                isRunning = false;
                setTimerValue(getResources().getString(R.string.label_timer_default));
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // handling configuration change
        startButton.setEnabled(!isRunning);
        stopButton.setEnabled(isRunning);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
        if (bound) {
            timerService.onApplicationAppear();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
        if (bound) {
            timerService.onApplicationHide();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bound) {
            getActivity().getApplication().unbindService(connection);
            bound = false;
        }
    }

    public void setTimerValue(String value) {
        timerLabel.setText(value);
    }

    private class TimerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TimerService.LocalBinder binder = (TimerService.LocalBinder) service;
            timerService = binder.getService();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
        }
    }

}
