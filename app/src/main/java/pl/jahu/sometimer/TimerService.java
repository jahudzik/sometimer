package pl.jahu.sometimer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

/**
 * SomeTimer
 * Created by jahudzik on 2015-03-09.
 */
public class TimerService extends Service {

    public static final String TIMER_UPDATE_ACTION = "timerUpdate";
    public static final String TIMER_VALUE = "timerValue";

    private static final int TIMER_NOTIFICATION_ID = 16;
    private static final int TIMER_NOTIFICATION_DELAY = 500; // 0.5 second
    private static final long TIME_LIMIT = 360000000; // 100 hours
    private static final int INTERVAL = 100; // 0.1 second

    private LocalBinder binder = new LocalBinder();
    private CountDownTimer timer;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationStarterThread notificationStarterThread;

    private volatile boolean showNotification;
    private volatile String lastTimeValue;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("SomeTimer running...");
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public IBinder onBind(Intent intent) {
        startCounting();
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        interruptNotification();
        showNotification = false;
        notificationManager.cancel(TIMER_NOTIFICATION_ID);
    }

    private void startCounting() {
        lastTimeValue = getResources().getString(R.string.label_timer_default);
        timer = new CountDownTimer(TIME_LIMIT, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                String timeValue = TimerUtils.formatMilliseconds(TIME_LIMIT - millisUntilFinished);
                if (!timeValue.equals(lastTimeValue)) {
                    if (showNotification) {
                        // TimerActivity is in the background - update notification
                        updateNotification(timeValue);
                    } else {
                        // TimerActivity is visible - update it
                        Intent intent = new Intent(TIMER_UPDATE_ACTION);
                        intent.putExtra(TIMER_VALUE, timeValue);
                        sendBroadcast(intent);
                    }
                    lastTimeValue = timeValue;
                }
            }

            @Override
            public void onFinish() {
                stopSelf();
            }
        };
        timer.start();
    }

    public void onApplicationAppear() {
        // activity appearing - hide notification
        interruptNotification();
        notificationManager.cancel(TIMER_NOTIFICATION_ID);
        showNotification = false;
        // reset lastTimeValue so timer will be updated instantly
        lastTimeValue = getResources().getString(R.string.label_timer_default);
    }

    public void onApplicationHide() {
        // activity hiding - wait with notification, ACTIVITY_APPEARING or APPLICATION_CLOSING event can come right away
        postNotification();
    }

    private void updateNotification(String timeValue) {
        Intent intent = new Intent(this, TimerActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.putExtra(TimerActivity.TIMER_VALUE_EXTRA_KEY, timeValue);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setContentText(timeValue);
        notificationManager.notify(TIMER_NOTIFICATION_ID, notificationBuilder.build());
    }

    private void postNotification() {
        notificationStarterThread = new NotificationStarterThread();
        notificationStarterThread.start();
    }

    private void interruptNotification() {
        if (notificationStarterThread != null && notificationStarterThread.isAlive()) {
            notificationStarterThread.interrupt();
        }
    }

    public class LocalBinder extends Binder {

        TimerService getService() {
            return TimerService.this;
        }

    }


    private class NotificationStarterThread extends Thread {

        @Override
        public void run() {
            boolean interrupted = false;
            try {
                // give a chance to configuration change or back button to interrupt and dismiss notification
                sleep(TIMER_NOTIFICATION_DELAY);
            } catch (InterruptedException e) {
                interrupted = true;
            }
            if (!interrupted) {
                // activity hiding - start showing notification
                showNotification = true;
                // reset lastTimeValue so notification will be updated instantly
                lastTimeValue = getResources().getString(R.string.label_timer_default);
            }
        }

    }

}
