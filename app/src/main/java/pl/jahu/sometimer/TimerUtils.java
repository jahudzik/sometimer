package pl.jahu.sometimer;

/**
 * SomeTimer
 * Created by jahudzik on 2015-03-09.
 */
public class TimerUtils {

    static String formatMilliseconds(long millis) {
        long seconds = (millis / 1000) % 60;
        long minutes = (millis / 60000) % 60;
        long hours = millis / 3600000;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

}
